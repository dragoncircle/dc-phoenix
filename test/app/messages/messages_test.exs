defmodule App.MessagesTest do
  use App.DataCase

  alias App.Messages

  describe "posts" do
    alias App.Messages.Post

    @valid_attrs %{action: "some action", author: "some author", content: "some content"}
    @update_attrs %{action: "some updated action", author: "some updated author", content: "some updated content"}
    @invalid_attrs %{action: nil, author: nil, content: nil}

    def post_fixture(attrs \\ %{}) do
      {:ok, post} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Messages.create_post()

      post
    end

    test "list_posts/0 returns all posts" do
      post = post_fixture()
      assert Messages.list_posts() == [post]
    end

    test "get_post!/1 returns the post with given id" do
      post = post_fixture()
      assert Messages.get_post!(post.id) == post
    end

    test "create_post/1 with valid data creates a post" do
      assert {:ok, %Post{} = post} = Messages.create_post(@valid_attrs)
      assert post.action == "some action"
      assert post.author == "some author"
      assert post.content == "some content"
    end

    test "create_post/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Messages.create_post(@invalid_attrs)
    end

    test "update_post/2 with valid data updates the post" do
      post = post_fixture()
      assert {:ok, post} = Messages.update_post(post, @update_attrs)
      assert %Post{} = post
      assert post.action == "some updated action"
      assert post.author == "some updated author"
      assert post.content == "some updated content"
    end

    test "update_post/2 with invalid data returns error changeset" do
      post = post_fixture()
      assert {:error, %Ecto.Changeset{}} = Messages.update_post(post, @invalid_attrs)
      assert post == Messages.get_post!(post.id)
    end

    test "delete_post/1 deletes the post" do
      post = post_fixture()
      assert {:ok, %Post{}} = Messages.delete_post(post)
      assert_raise Ecto.NoResultsError, fn -> Messages.get_post!(post.id) end
    end

    test "change_post/1 returns a post changeset" do
      post = post_fixture()
      assert %Ecto.Changeset{} = Messages.change_post(post)
    end
  end
end

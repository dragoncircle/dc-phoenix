defmodule App.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :author, :string
      add :content, :string
      add :action, :string

      timestamps()
    end

  end
end

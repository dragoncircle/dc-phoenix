defmodule App.Messages.Post do
  use Ecto.Schema
  import Ecto.Changeset
  alias App.Messages.Post


  schema "posts" do
    field :action, :string
    field :author, :string
    field :content, :string

    timestamps()
  end

  @doc false
  def changeset(%Post{} = post, attrs) do
    post
    |> cast(attrs, [:author, :content, :action])
    |> validate_required([:author, :content, :action])
  end
end
